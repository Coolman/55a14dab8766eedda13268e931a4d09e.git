(() => {
    const packName = 'com.taobao.litetao',
        mainAct = 'com.taobao.ltao.maintab.MainFrameActivity',
        browAct = 'com.taobao.ltao.browser.BrowserActivity',
        browUpAct = 'com.taobao.ltao.browser.ext.BrowserUpperActivity',
        liveAct = 'com.taobao.ltao.live.TaoLiveVideoActivity',
        searchAct = 'com.taobao.search.sf.srp.MainSearchResultActivity',
        splashAct = 'com.taobao.splash.SplashActivity';
    const defautWaitTime = 3000,
        renderWaitTime = 1000,
        taskWaitTime = 16000,
        liveWaitTime = 31000;
    const progressReg = /(\d{1,2})\/(\d{1,2})/;
    let isLive = false;
    console.log(currentPackage());
    if (packName != currentPackage) {
        let t = threads.start(function() {
            if (!launch(packName)) {
                toast('应用未找到');
                exit();
                return;
            }
            waitForPackage(packName);
        });
        t.join(defautWaitTime);
    }
    console.log(currentActivity());
    const inMainAct = () => {
        const btn = textContains('赚特币').findOne();
        btn.click();
        waitForActivity(browAct);
        checkCurAct();
    };
    const checkIsLive = (uiObj) => {
        return uiObj.text().includes('边看边买')
            || null != uiObj.parent().findOne(textContains('30秒'))
            || null != uiObj.parent().findOne(textContains('看直播'));
    };
    const inBrowAct = () => {
        sleep(2 * renderWaitTime); // wait for rendering
        let elem = textContains('今日厂家直降').findOnce();
        if (elem) {
            inBrowUpAct();
            return;
        }
        const zbBtn = textEndsWith('143.png_').findOnce();
        console.log('zbBtn', zbBtn);
        if (zbBtn) {
            zbBtn.click();
        } else {
            toastLog('not found <赚币>');
            exit();
        }
        text("赚币中心").waitFor();
        sleep(renderWaitTime); // wait for rendering
        for (let i = textContains('0/1').findOnce(); i != null;) {
            console.log(i.text());
            let found = i.text().match(progressReg);
            if (!found) {
                toastLog('check progress error');
                continue;
            }
            let [done, all] = [+found[1], +found[2]];
            if (done < all) {
                isLive = checkIsLive(i);
                i.click();
                sleep(renderWaitTime);
                checkCurAct();
            } else {
                toastLog('may done');
                continue;
            }
            i = textContains('0/1').findOnce();
        }
        toastLog('not found tasks, may all done');
        exit();
    };
    const inBrowUpAct = () => {
        sleep(renderWaitTime);
        if (!isLive) {
            let x2 = random(400, 350), y2 = random(200, 280),
                deltaX = random(-200, 200), deltaY = random(200, 500);
            let x1 = x2 + deltaX, y1 = y2 + deltaY;
            let ranMs = random(500, 800);
            swipe(x1, y1, x2, y2, ranMs);
            sleep(taskWaitTime);
        } else {
            isLive = false;
            sleep(liveWaitTime);
        }
        let nav = descStartsWith('Navigate').findOnce();
        console.log(nav);
        (nav && nav.click()) || back();
        sleep(renderWaitTime);
        checkCurAct();
    };
    const inSplashAct = () => {
        const skipBtn = textStartsWith('还剩').findOnce();
        skipBtn && skipBtn.click();
        checkCurAct();
    };
    const checkCurAct = () => {
        switch(currentActivity()) {
            case mainAct:
                console.log('case mainAct');
                inMainAct();
                break;
            case browAct:
                console.log('case browAct');
                inBrowAct();
                break;
            case liveAct:
            case searchAct:
            case browUpAct:
                console.log('case task execution');
                inBrowUpAct();
                break;
            case splashAct:
                console.log('case splashAct');
                inSplashAct();
                break;
            default:
                console.log('case default', currentActivity());
                break;
        };
    }
    checkCurAct();
})();
