// @author    Coolman <QQ Group ID: 892850160>
// 给 <User-Agent Switcher and Manager> 添加自定义 UA
(() => {
    const props = [
        'appVersion',
        'platform',
        'vendor',
        'product',
        'oscpu',
        'userAgent',
        'custom-variable',
    ];
    const exclusions = [
        // 'vendor',
    ];
    let myCustomUseragent = {};
    for (let p in navigator) {
        (props.includes(p) && (myCustomUseragent[p] = navigator[p]))
            || (exclusions.includes(p) && (myCustomUseragent[p] = '[delete]'));
    }
    console.log(myCustomUseragent);
    console.info(JSON.stringify({'my-custom-useragent': myCustomUseragent}));
})();
