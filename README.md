## 脚本说明

> 搬运、转载请注明出处 <Coolman https://www.52pojie.cn/?357683 >。

可能有的人看不懂不会用，在这里简单说明下用处吧。

如果你不知道什么是“油猴脚本”、“auto.js”，去网上搜。

* [inject-oncopy.js](./inject-oncopy.js).

  auto.js 脚本。

  还给我的网页复制功能。（segmentfault.com yuque.com）

* [litetao_autojs.js](./litetao_autojs.js).

  auto.js 脚本。

  淘宝特价版浏览任务。

* [zha-nianshou_autojs_by-anonymous.js](./zha-nianshou_autojs_by-anonymous.js)

  auto.js 脚本。

  京东炸年兽活动。一位不愿透露名字的大哥私聊发给我的。

* [zha-nianshou_autojs_by-anonymous_latest.js](./zha-nianshou_autojs_by-anonymous_latest.js)

  auto.js 脚本。

  京东炸年兽活动。一位不愿透露名字的大哥私聊发给我的。

  （还是大哥的，最新版本。）

* [get-plus-coupon.js](./get-plus-coupon.js)

  油猴脚本。

  "PLUS专属百亿补贴" https://u.jd.com/x3cOWK 。

* [get-cart.js](./get-cart.js)

  油猴脚本。

  本来写了双十一抢天猫精灵用的，改改或许能抢其他东西。

* ~~[get-maotai.js](./get-maotai.js)~~

  油猴脚本。

  半残品，没法用，想抢茅台的，跟抢天猫精灵同理。

* [get-free-coupons.js](./get-free-coupons.js)

  油猴脚本。

  双十一京东定时抢免费券用的。

* [get-jd-coupon.js](./get-jd-coupon.js)

  油猴脚本。

  双十一京东百亿补贴活动抢九块九鸡蛋用的。

  52 帖子（https://www.52pojie.cn/thread-1049157-1-1.html ） 。

* [zha-nianshou.js](./zha-nianshou.js)

  油猴脚本。

  京东炸年兽，需要改浏览器 UA 或者借助其他扩展。

  详情见 52 的帖子（https://www.52pojie.cn/thread-1085170-1-1.html ） 吧。

* [zha-nianshou_autojs.js](./zha-nianshou_autojs.js)

  auto.js 脚本。

  京东炸年兽活动，直接跑在手机上的。

  自己维护的版本，不限制 Android 7.0 + 了。

* [get-custom-ua-4-ua-switcher-and-manager.js](./get-custom-ua-4-ua-switcher-and-manager.js)

  给 **User-Agent Switcher and Manager** 添加自定义 UA。

如果你有想法，可以一起合作。

## 成果物

### ~~[`be5a696`](https://gitee.com/Coolman/55a14dab8766eedda13268e931a4d09e/tree/be5a6968dfd414b143b2f934c31d59b219972f25)~~

* ~~[zha-nianshou_be5a696.apk](./zha-nianshou_be5a696.apk)~~.

  sha1sum:

  ```
  af948460812cff856d6f533671abb1834245c061  zha-nianshou_be5a696.apk
  ```

### latest

* [zha-nianshou_latest.apk](./zha-nianshou_latest.apk).

  Build upon [zha-nianshou_autojs_by-anonymous_latest.js](./zha-nianshou_autojs_by-anonymous_latest.js).

  sha1sum:

  ```
  ec6f692b1abb5fe1a762c12ae427b7eed1d62972  zha-nianshou_latest.apk
  ```
