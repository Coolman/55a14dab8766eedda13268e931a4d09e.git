(() => {
    const sleep = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    };
    const af = async () => {
        for (let i = 0; i < 20; ++i) {
            console.log('before sleep', i);
            await sleep(1200);
            console.log('after sleep', i);
        }
    };
    af();
})();
